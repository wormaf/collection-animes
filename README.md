# Collection animé
***
## Bienvenue sur le README de notre site web.
![Image text](https://cdn.discordapp.com/attachments/894620916661764136/899322169010577448/beau.PNG)
## Pour Linux
***

### Pour la lancer il faut tout d'abord:
* Lancer un terminal dans le fichier /www avec la commande:
```
$ php -S localhost:8000
```
* Lancer un terminal dans le meme fichier avec la commande:
```
$ mysql -h servinfo-mariadb -u "Votre nom d'utilisateur de MariaDB" -p
```
* Se connecter avec le mot de passe
* Sélectionner une base de données avec la commande:
```
$ use "nom de la databases";
```
* Créer la base de données avec la commande:
```
$ source Creation_Table.sql;
```
* Modifier le fichier connect.php avec les bons identifiant:
![Image text](https://cdn.discordapp.com/attachments/852847873678639126/899320387047923762/unknown.png)
* Enfin aller sur votre navigateur et ecrire dans la barre de recherche "localhost:8000/Accueil.php"
***

## Pour Windows
***

### Pour la lancer il faut tout d'abord:
* Lancer wampserver (ou xampp):
* écrire localhost dans la barre de recherche et selctionner phpMyAdmin
* Se connecter avec les bons identifiants
![image text](https://cdn.discordapp.com/attachments/894620916661764136/899325467671277699/unknown.png)
* Importer la base de données Creation_Table.sql dans phpMyAdmin
* Changer les identifiants si nécessaire dans connect.php
![Image text](https://cdn.discordapp.com/attachments/852847873678639126/899320387047923762/unknown.png)
* Enfin aller sur votre navigateur et ecrire dans la barre de recherche "localhost/"Le bon parcours"/Accueil.php"
***