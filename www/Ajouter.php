<body><main><h1>Ajouter des animes</h1>
    <?php
    include('inc/header.php');
    include('inc/nav.php');
    require("connection.php");
    $connexion = connect_bd();
    $sql1="SELECT * from REALISATEUR";
    $sql2="SELECT * from STUDIO";

        if(!$connexion->query($sql1) and $connexion->query($sql2) )   echo "Pb d'accès aux ANIME";
    else {
    ?>

    <form method="GET" action="AjouterV2.php">
        <p>Titre: <input type="text" name="titreAjouter"/></p>
        <p>Année de début: <input type="text" name="anneeDeDebutAjouter"/></p>
        <p>Genre: <input type="text" name="GenreAjouter"/></p>
        <p><HR NOSHADE></p>
        <p>Nom du Réalisateur: 
        <select name="realisateurAjouter">
        <?php
        foreach ($connexion->query($sql1) as $row)
            if(!empty($row['idRealisateur']))
            echo "<option value='".$row['idRealisateur']."'>"
            .$row['nomRealisateur']." ".$row['prenomRealisateur']."</option>\n";
      ?>
        </select>
        </p>
        <p><HR NOSHADE></p>
        <p>Nom du Studio: 
        <select name="studioAjouter">
        <?php
        foreach ($connexion->query($sql2) as $row)
            if(!empty($row['idStudio']))
            echo "<option value='".$row['idStudio']."'>"
            .$row['nomStudio']."</option>\n";
      ?>
        </select>
        </p>
    <div class='bouton'>
        <button type="submit"> Ajouter</button>
        <button type="reset"> Reset</button>
    </div>

    <?php }?>
</main>
</body>
</html>