<nav class="navbar">
      <ul class="navbar-menu">
        <li class="navbar-item logo">
          <a href="#" class="navbar-link">
            <span class="navbar-title">Menu Déroulant</span>
            <i class="fas fa-chevron-right navbar-icon"></i>
          </a>
        </li>
        <li class="navbar-item">
          <a href="Accueil.php" class="navbar-link">
            <i class="fas fa-home navbar-icon"></i>
            <span class="navbar-title">Accueil</span>
          </a>
        </li>
        <li class="navbar-item">
          <a href="Ajouter.php" class="navbar-link">
            <i class="fas fa-plus navbar-icon"></i>
            <span class="navbar-title">Ajouter</span>
          </a>
        </li>
        <li class="navbar-item">
          <a href="Supprimer.php" class="navbar-link">
            <i class="fas fa-minus navbar-icon"></i>
            <span class="navbar-title">Supprimer</span>
          </a>
        </li>
        <li class="navbar-item">
          <a href="Modifier.php" class="navbar-link">
            <i class="fas fa-backward navbar-icon"></i>
            <span class="navbar-title">Modifier</span>
          </a>
        </li>
        <li class="navbar-item">
          <a href="AjouterRealisateur.php" class="navbar-link">
            <i class="fas fa-address-card navbar-icon"></i>
            <span class="navbar-title">Ajouter Realisateur</span>
          </a>
        </li>
        <li class="navbar-item">
          <a href="AjouterStudio.php" class="navbar-link">
            <i class="fas fa-building navbar-icon"></i>
            <span class="navbar-title">Ajouter Studio</span>
          </a>
        </li>
      </ul>
    </nav>