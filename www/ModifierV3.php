<body>
<main>

    <h1>Modifier des animes</h1></body>
    <?php
    include('inc/header.php');
    include('inc/nav.php');
    try{
        require("connection.php");
        $connexion = connect_bd();  
    }    
    catch(PDOException $e){
        echo "Erreur : " . $e->getMessage();
    }
    
    if(isset($_GET["anneeDeDebutModifier"]) and isset($_GET["GenreModifier"])){

        $sql=$_GET['studioModifier'];
        
        $sql2=$_GET['realisateurModifier'];
        
        $sqlT=$_GET['titreModifier'];
        
        $sqlA=$_GET['anneeDeDebutModifier'];
        $sqlG=$_GET['GenreModifier'];
        
        $requete=$connexion->prepare("UPDATE ANIME set idStudio=:nvStudio, idRealisateur=:nvRealisateur, anneeDebut=:nvAnnee, Genre=:nvgenre where Titre=:leTitre");
        $requete->execute(array(
            'nvStudio'=>$sql, 
            'nvRealisateur'=>$sql2, 
            'nvAnnee'=>$sqlA, 
            'nvgenre'=>$sqlG,
            'leTitre'=>$sqlT));
        $requete->closeCursor();
        
        echo "Animé ".$_GET['titreModifier'].' modifié';
        
        
        }
        ?>
        
        <ul>
            
            <li><a href="Accueil.php">Retour au Menu principal</a></li>
        </ul>

</main>
</body>
</html>