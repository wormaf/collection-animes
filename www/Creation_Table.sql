DROP TABLE ANIME;
DROP TABLE REALISATEUR;
DROP TABLE STUDIO;


CREATE TABLE ANIME (
	titre varchar(20),
	idRealisateur int,
	idStudio int,
	anneeDebut date,
    genre varchar(20),
	PRIMARY KEY(titre)
);
CREATE TABLE REALISATEUR (
	idRealisateur INT NOT NULL AUTO_INCREMENT,
	nomRealisateur varchar(20),
	prenomRealisateur varchar(20),
	PRIMARY KEY(idRealisateur)
);
CREATE TABLE STUDIO (
	idStudio INT NOT NULL AUTO_INCREMENT,
	nomStudio varchar(20),
	PRIMARY KEY(idStudio)
);
ALTER TABLE ANIME ADD FOREIGN KEY(idRealisateur) REFERENCES  REALISATEUR (idRealisateur);
ALTER TABLE ANIME ADD FOREIGN KEY(idStudio) REFERENCES  STUDIO (idStudio);


INSERT INTO REALISATEUR(nomRealisateur, prenomRealisateur) values ('Uda','Konosuke'),
('Date','Hayato'),
('Abe','Noriyuki'),
('Nagasaki','Kenji'),
('Araki','Tetsuro'),
('Taniguchi','Goro'),
('Okazaki','Minoru'),
('Gotoge','Koyoharu'),
('Miura','Takashiro'),
('Park','Sunghoo'),
('Ito','Tomohiko'),
('Itagaki','Shin'),
('Sato','Takuya'),
('Morita','Shuhei'),
('Ibata','Yoshihide');

INSERT INTO STUDIO(nomStudio) values ('Toei'),('Pierrot'),('Bones'),('WIT'),('Madhouse'),('Sunrise'),('Ufotable'),('Mappa'),('A1 Pictures'),('Gemba'),('White fox'),('Deen');

INSERT INTO ANIME values ('One Piece',1,1,'1997-07-22','Shonen');
INSERT INTO ANIME values ('Naruto',2,2,'2002-10-03','Shonen');
INSERT INTO ANIME values ('Bleach',3,2,'2004-10-05','Shonen');
INSERT INTO ANIME values ('My Hero Academia',4,3,'2014-07-07','Shonen');
INSERT INTO ANIME values ('Shingeki no Kyojin',5,4,'2009-09-09','Shonen');
INSERT INTO ANIME values ('Death Note',5,5,'2009-09-09','Shonen');
INSERT INTO ANIME values ('Code Geass',6,6,'2006-10-05','Seinen');
INSERT INTO ANIME values ('Dragon Ball',7,1,'1986-02-26','Shonen');
INSERT INTO ANIME values ('Demon Slayer',8,7,'2019-02-04','Shonen');
INSERT INTO ANIME values ('Fate/Stay night: UBW',9,7,'2014-10-04','Seinen');
INSERT INTO ANIME values ('Jujutsu Kaisen',10,8,'2020-10-03','Shonen');
INSERT INTO ANIME values ('Erased',11,9,'2016-01-07','Seinen');
INSERT INTO ANIME values ('Berserk',12,10,'2016-07-01','Seinen');
INSERT INTO ANIME values ('Steins Gate',13,11,'2016-07-01','Seinen');
INSERT INTO ANIME values ('Tokyo Ghoul',14,2,'2014-07-04','Seinen');
INSERT INTO ANIME values ('Fruits Basket',15,12,'2019-04-06','Shojo');